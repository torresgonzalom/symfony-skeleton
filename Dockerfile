FROM php:8.1.6-fpm-alpine
WORKDIR /app

RUN apk --no-cache add postgresql-dev postgresql-client git bash

RUN docker-php-ext-install pdo_pgsql

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN curl -sS https://get.symfony.com/cli/installer | bash -s - --install-dir /usr/local/bin
